﻿if(-not (Test-Path 'p4merge_vs.exe')) {
    Write-Warning "Could not locate p4merge_vs.exe"
    Exit
}

if(-not (Test-Path 'p4merge.exe')) {
    Write-Warning "Could not locate p4merge.exe"
    Exit
}

$p4merge_vs_path = Join-Path . 'p4merge_vs.exe' -Resolve
$compare = @{
    Command = $p4merge_vs_path;
    Arguments = '%1 %2 "" "" "" %6 %7 "" ""';
}

$merge = @{
    Command = $p4merge_vs_path;
    Arguments = '%1 %2 %3 %4 "" %6 %7 %8 %9';
}

Push-Location HKCU:\Software\Microsoft\VisualStudio\
gci | %{
  cd (Split-Path $_ -Leaf)
  if(Test-Path TeamFoundation\SourceControl\DiffTools) {
    cd TeamFoundation\SourceControl\DiffTools
    New-Item .* -Force | Out-Null
    cd .*
    "Updating $(Get-Location)"
    New-Item Compare -Force | Out-Null
    New-Item Merge -Force | Out-Null
    New-ItemProperty Compare -Name Command -PropertyType String -Value $compare.Command -Force | Out-Null
    New-ItemProperty Compare -Name Arguments -PropertyType String -Value $compare.Arguments -Force | Out-Null
    New-ItemProperty Merge -Name Command -PropertyType String -Value $merge.Command -Force | Out-Null
    New-ItemProperty Merge -Name Arguments -PropertyType String -Value $merge.Arguments -Force | Out-Null
    cd ..\..\..\..
  }
  cd ..
}
Pop-Location
<#
Set-Location HKCU:\Software\Microsoft\VisualStudio\12.0\TeamFoundation\SourceControl\DiffTools
New-Item .*
New-Item .*\Compare
New-Item .*\Merge
New-ItemProperty .*\Compare -Name Command -PropertyType String -Value $compare.Command
New-ItemProperty .*\Compare -Name Arguments -PropertyType String -Value $compare.Arguments
New-ItemProperty .*\Merge -Name Command -PropertyType String -Value $merge.Command
New-ItemProperty .*\Merge -Name Arguments -PropertyType String -Value $merge.Arguments
#>