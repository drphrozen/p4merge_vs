﻿using System;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using p4merge_vs_install.Properties;

namespace p4merge_vs_install
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            var perforcePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), @"Perforce\");
            var p4MergePath = LocateP4Merge(Directory.Exists(perforcePath) ? perforcePath : Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles));
            if (p4MergePath == null || !File.Exists(p4MergePath))
            {
                WriteLine("Could not locate p4merge!");
            }
            else
            {
                var p4MergeVsPath = Path.Combine(Path.GetDirectoryName(p4MergePath), "p4merge_vs.exe");
                try
                {
                    WriteLine("Create " + p4MergeVsPath);
                    CreateP4MergeVs(p4MergeVsPath);
                    try
                    {
                        var visualStudioKey = Open(Registry.CurrentUser, @"Software\Microsoft\VisualStudio");
                        if (visualStudioKey == null) return;
                        foreach (var subKeyName in visualStudioKey.GetSubKeyNames())
                        {
                            var key = Open(visualStudioKey,
                                string.Format(@"{0}\TeamFoundation\SourceControl\DiffTools", subKeyName));
                            if (key == null) continue;
                            key = key.CreateSubKey(".*");
                            WriteLine("Updating " + key.Name.Replace("HKEY_CURRENT_USER", "HKCU"));
                            var compareKey = key.CreateSubKey("Compare");
                            compareKey.SetValue("Command", p4MergeVsPath);
                            compareKey.SetValue("Arguments", @"%1 %2 """" """" """" %6 %7 """" """"");
                            var mergeKey = key.CreateSubKey("Merge");
                            mergeKey.SetValue("Command", p4MergeVsPath);
                            mergeKey.SetValue("Arguments", @"%1 %2 %3 %4 """" %6 %7 %8 %9");
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteError("Could not update registry for diff tools!", ex);
                    }
                }
                catch (Exception ex)
                {
                    WriteError("Could not create p4merge_vs.exe!", ex);
                }
            }
            WriteLine("Done.. Press any key to continue.");
            Console.ReadKey();
        }

        private static string LocateP4Merge(string initialDirectory)
        {
            var dialog = new OpenFileDialog
            {
                Filter = "P4Merge|p4merge.exe|All files (*.*)|*.*",
                RestoreDirectory = false,
                CheckFileExists = true,
                CheckPathExists = true,
                Multiselect = false,
                InitialDirectory = initialDirectory
            };
            return dialog.ShowDialog() == DialogResult.OK
                ? dialog.FileName
                : null;
        }

        private static void WriteLine(string line)
        {
            Console.WriteLine(line);
        }

        private static void WriteError(string line, Exception ex)
        {
            Console.WriteLine("ERROR: {0}\r\n\r\n{1}", line, ex);
        }

        private static void CreateP4MergeVs(string p4MergePath)
        {
            using (var stream = File.Create(p4MergePath))
            {
                CopyStream(new MemoryStream(Resources.p4merge_vs), stream);
            }
        }

        private static void CopyStream(Stream input, Stream output)
        {
            var buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }

        private static RegistryKey Open(RegistryKey registryKey, string path)
        {
            var keys = path.Trim(new[] { '\\' }).Split(new[] { '\\' });

            foreach (var key in keys)
            {
                if (registryKey == null) return null;
                registryKey = registryKey.OpenSubKey(key, true);
            }
            return registryKey;
        }
    }
}
