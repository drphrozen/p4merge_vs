﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace p4merge_vs
{
    class Program
    {
        static void Main(string[] args)
        {
            Execute(new string[] {null}.Concat(args).ToArray());
        }

        private static void Execute(IList<string> args)
        {
            try
            {
                var argsAsObjects = args.Select(x => string.IsNullOrEmpty(x) ? null : '"' + x.Replace("\"", "\"\"") + '"')
                            .Cast<object>()
                            .ToArray();
                if (argsAsObjects.Length < 10)
                    throw new ArgumentException("9 arguments was expected!");
                var isDiff = argsAsObjects[4] == null;
                var arguments = isDiff
                    ? string.Format(@"-nl {6} -nr {7} {1} {2}", argsAsObjects)
                    : string.Format(@"-nb {8} -nl {6} -nr {7} -nm {9} {3} {1} {2} {4}", argsAsObjects);
                if (!isDiff)
                {
                    File.Create(args[4]).Close();
                }
                var process = new Process
                {
                    StartInfo =
                    {
                        WorkingDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                        FileName = "p4merge.exe",
                        Arguments = arguments,
                        UseShellExecute = true
                    }
                };
                process.Start();
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "p4merge_vs exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}